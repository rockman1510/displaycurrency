- This project build to display rates currency (usd - pln) change per 1 minute.
- The project can run on android device from version 7.0 to higher.
- The project applied offline build gradle with file /gradle/wrapper/gradle-5.6.2-bin.zip.
- The project applied some library jar file such as:
    + Google GSON 2.8.5 for converting from Json to Gson Object and using annotation in parsing JSON Object.
    + OkHttp 4.2.0 and Okio 2.4.0 for making http request to get currency rates from url.
- The project run only on a screen built by an activity with some TextViews.
- The project only use Internet permission for connecting network for application.
- The project created in MVP structure:
    + Model: To define the data of JSON Object that responses from url.
    + View: To display all data from Model to user interface (Activity and Views).
    + Presenter: To control, handle, make request data from url, parse data into Model and then transfer it to View for displaying.