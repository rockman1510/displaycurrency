package com.example.displaycurrency.model

import com.example.displaycurrency.Constants
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rates {

    @SerializedName(Constants.USD)
    @Expose
    var usd: Float = 0.0F

    @SerializedName(Constants.PLN)
    @Expose
    var pln: Float = 0.0F
}