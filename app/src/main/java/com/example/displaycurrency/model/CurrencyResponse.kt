package com.example.displaycurrency.model

import com.example.displaycurrency.Constants
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CurrencyResponse {

    @SerializedName(Constants.RATES)
    @Expose
    var rates: Rates? = null
}
