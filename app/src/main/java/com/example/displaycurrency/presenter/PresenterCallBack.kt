package com.example.displaycurrency.presenter

import com.example.displaycurrency.model.Rates

interface PresenterCallBack {
    fun onSuccess(rate: Rates)
    fun onError(message: String)
}