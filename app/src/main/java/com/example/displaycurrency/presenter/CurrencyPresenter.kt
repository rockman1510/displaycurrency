package com.example.displaycurrency.presenter

import android.app.Activity
import com.example.displaycurrency.Constants
import com.example.displaycurrency.model.CurrencyResponse
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException

class CurrencyPresenter(private val activity: Activity, private val callback: PresenterCallBack?) {

    fun getCurrencyApi() {
        val okHttpClient = OkHttpClient()
        val request = Request.Builder().url(Constants.ADDRESS_URL).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback!!.onError(e.message.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                activity.runOnUiThread {
                    if (response.isSuccessful) {
                        val currencyResponse =
                            Gson().fromJson(response.body!!.string(), CurrencyResponse::class.java)
                        callback!!.onSuccess(currencyResponse.rates!!)
                    } else {
                        callback!!.onError(response.message)
                    }
                }
            }
        })
    }
}