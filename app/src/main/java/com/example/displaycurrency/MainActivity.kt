package com.example.displaycurrency

import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.example.displaycurrency.model.Rates
import com.example.displaycurrency.presenter.CurrencyPresenter
import com.example.displaycurrency.presenter.PresenterCallBack
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), PresenterCallBack {

    var countDownTimer: CountDownTimer? = null
    var presenter: CurrencyPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = CurrencyPresenter(this, this)
        presenter!!.getCurrencyApi()
        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                countDownTimer!!.start()
                presenter!!.getCurrencyApi()
            }
        }
        countDownTimer!!.start()
    }

    override fun onStop() {
        super.onStop()
        countDownTimer!!.cancel()
    }

    override fun onSuccess(rate: Rates) {
        usdValue.text = rate.usd.toString()
        plnValue.text = rate.pln.toString()
        val dateFormat = SimpleDateFormat("HH:MM:SS dd-MM-yyyy", Locale.getDefault())
        updateValue.text = dateFormat.format(Date(System.currentTimeMillis())).toString()
    }

    override fun onError(message: String) {
        updateValue.text = message
    }
}
