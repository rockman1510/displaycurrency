package com.example.displaycurrency

object Constants {
    const val ADDRESS_URL = "https://api.exchangeratesapi.io/latest"
    const val RATES = "rates"
    const val USD = "USD"
    const val PLN = "PLN"
}